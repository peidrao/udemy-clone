
from django.urls import path

from .views import ProfileCreate, ProfileList, ProfileMe

urlpatterns = [
    path('profiles/', ProfileList.as_view(), name='profiles_list'),
    path('me/', ProfileMe.as_view(), name='profile_me'),
    path('signup/', ProfileCreate.as_view(), name='profiles_create'),
]
