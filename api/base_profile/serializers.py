
from rest_framework import serializers

from .models import Profile



class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('id', 'last_login', 'picture', 'username', 'full_name', 'email', 'phone', 'type_user')


class ProfileCreateSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Profile
        fields = '__all__'


    def create(self, validated_data):
       instance = super(ProfileCreateSerializer, self).create(validated_data)
       instance.set_password(validated_data.get('password'))
       instance.is_active = True
       instance.save()
       return instance
