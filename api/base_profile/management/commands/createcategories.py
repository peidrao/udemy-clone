from django.core.management.base import BaseCommand

from base_course.models import Category


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        categories = [
                {'name': 'Desenvolvimento', 
                    'childs': [
                        {'name': 'Desenvolvimento Web'}, 
                        {'name': 'Desenvolvimento de Games'},
                        {'name': 'Teste de Software'},
                        {'name': 'Desenvolvimento mobile'}]},
                {'name': 'Negócios'},
                {'name': 'Finanças e contabilidade'},
                {'name': 'TI e software', 'childs': [
                        {'name': 'Redes e infraestrutura'},
                        {'name': 'Hardware'},
                ]},
                {'name': 'Produtividade no escritório'},
                {'name': 'Desenvolvimento Pessoal'},
                {'name': 'Design'},
                {'name': 'Marketing'},
                {'name': 'Saúde e fitness'},
                ]

        for category in categories:
            if not Category.objects.filter(name=category['name']).exists():
                category_object = Category.objects.create(name=category['name'])
                if category.get('childs'):
                    for child in category.get('childs'):
                        if not Category.objects.filter(name=child['name']).exists():
                            Category.objects.create(name=child['name'], origin=category_object)


