from django.db import models
from django.contrib.auth.models import AbstractBaseUser, UserManager
from django.utils.translation import gettext_lazy as _
from django.utils import timezone


class CustomUserManager(UserManager):
    def _create_user(self, username, email, password,
                     is_staff, is_superuser, **extra_fields):

        now = timezone.now()
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(username=username,
                          email=email,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          created_at=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username,email, password, **extra_fields):
        return self._create_user(username, email, password, True, True,
                                 **extra_fields)


class Profile(AbstractBaseUser):
    class TypeUser(models.IntegerChoices):
        TEACHER = 1, _('Teacher')
        STUDENT = 2, _('Student')

    full_name = models.CharField(max_length=150)
    picture = models.ImageField(upload_to='images/profile', blank=True, null=True)
    username = models.CharField(max_length=100, unique=True, null=True, blank=True)
    phone = models.CharField(max_length=45, null=True)
    email = models.EmailField(
        _('email address'), unique=True, blank=False, null=False)
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates whether the user can log into this admin '
                                               'site.'))
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(_('active'), default=False,
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))
    created_at = models.DateTimeField(_('date joined'), auto_now_add=True)
    updated_at = models.DateTimeField(_('date updated'), default=timezone.now)
    deleted_at = models.DateTimeField(null=True)

    type_user = models.IntegerField(choices=TypeUser.choices, default=TypeUser.STUDENT)
    
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    
    objects = CustomUserManager()

    class Meta:
        swappable = 'AUTH_USER_MODEL'
        verbose_name = _('profile')
        verbose_name_plural = _('profiles')
    
    def __str__(self) -> str:
        return self.email

