import pytest
from django.urls import reverse
from model_bakery import baker

from base_profile.models import Profile

profiles_list = reverse("profiles_list")
profile_me = reverse("profile_me")
token_url = reverse('token_obtain_pair')
profiles_create = reverse('profiles_create')
pytestmark = pytest.mark.django_db

@pytest.fixture
def profile() -> Profile:
    profile =  baker.make(Profile, full_name='Test', is_active=True)
    profile.set_password('123')
    profile.save()
    return profile

@pytest.fixture
def token(client, profile) -> dict:
    payload = {'email': profile.email, 'password': '123'}
    response = client.post(path=token_url, data=payload, format='json')
    token = response.data.get('access')
    headers = {'HTTP_AUTHORIZATION': f'Bearer {token}'}
    return headers

def test_list_profiles_empty(client) -> None:
    response = client.get(profiles_list)
    # assert response.data['results'] == []
    assert response.status_code == 200

def test_list_profiles(client, profile) -> None:
    response = client.get(profiles_list)
    assert response.data['count'] == 1
    assert response.status_code == 200

def test_get_profile_me(client, profile, token) -> None:
    response = client.get(profile_me, **token)
    assert response.status_code == 200
    assert response.data.get('full_name') == profile.full_name

def test_get_profile_me_not_user(client) -> None:
    response = client.get(profile_me)
    assert response.status_code == 404

def test_post_profile(client) -> None:
    payload = {
	    "email": "pedro@gmail.com",
	    "full_name": "Pedro Fonseca",
	    "password": "@test",
	    "type_user": 1
    }

    response = client.post(profiles_create, payload)
    assert response.data['full_name'] == 'Pedro Fonseca'
    assert response.data['email'] == 'pedro@gmail.com'
    assert response.status_code == 201

def test_post_profile_email_invalid(client) -> None:
    payload = {
	    "email": "pedroad",
	    "full_name": "Pedro Fonseca",
	    "password": "@test",
	    "type_user": 1
    }
    response = client.post(profiles_create, payload)
    assert response.status_code == 400

def test_post_profile_password_is_short(client) -> None:
    payload = {
	    "email": "pedroad@gmail.com",
	    "full_name": "Pedro Fonseca",
	    "password": "@",
	    "type_user": 1
    }
    response = client.post(profiles_create, payload)
    assert response.status_code == 400
