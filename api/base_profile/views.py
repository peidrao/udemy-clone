from rest_framework import generics, status, views
from rest_framework.response import Response
from django.core.exceptions import ValidationError
from django import forms

from .serializers import ProfileCreateSerializer, ProfileSerializer
from .models import Profile


class ProfileList(generics.ListAPIView):
    queryset = Profile.objects.all().order_by('full_name')
    serializer_class = ProfileSerializer


class ProfileCreate(generics.CreateAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileCreateSerializer

    def create(self, request, *args, **kwargs):
        email = request.data.get('email')
        password = request.data.get('password')

        try:
            email_valid = forms.EmailField()
            email_valid.clean(email)
        except ValidationError as err:
            return Response({'error': err.messages}, status=status.HTTP_400_BAD_REQUEST)
        
        if not password or len(password) < 5:
            return Response({'error': 'Senha em branco ou menor que 5'}, status=status.HTTP_400_BAD_REQUEST)
        
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProfileMe(views.APIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

    def get(self, request):
        try:
            profile = self.queryset.get(id=request.user.id)
        except Profile.DoesNotExist:
            return Response({'message': 'user not found'}, status=status.HTTP_404_NOT_FOUND)

        serializer = self.serializer_class(profile)
        return Response(serializer.data)
