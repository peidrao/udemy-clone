from rest_framework import permissions, views, status
from rest_framework.response import Response

from base_checkout.models import Cart, Payment
from base_checkout.serializers import CartSerializer, PaymentSerializer
from django.db.models import Q, Sum

from base_course.models import Course


class CartView(views.APIView):
    queryset = Cart.objects.all()
    serializer_class = CartSerializer
    permission_classes = (permissions.IsAuthenticated,)
    
    def post(self, request):
        query = Q(profile=request.user, is_pendent=True)
        try:
            course = Course.objects.get(id=request.data.get('course'))
        except Course.DoesNotExist:
            return Response({'message': 'curso não foi encontrado'}, status=status.HTTP_404_NOT_FOUND)

        if self.queryset.filter(query).exists():
            cart = self.queryset.get(query)
        else:
            cart = self.queryset.create(profile=request.user)
        
        if request.data.get('remove') :
            if cart.courses.filter(id=course.id).exists():
                cart.courses.remove(course)
            else:
                return Response({'message': 'curso não existe no carrinho'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            cart.courses.add(course)
        
        serializer = self.serializer_class(cart)
        return Response(serializer.data)


    def get(self, request):
        try:
            queryset = self.queryset.get(profile=request.user, is_pendent=True)
        except Cart.DoesNotExist:
            return Response({'message': 'o carrinho está vázio'}, status=status.HTTP_400_BAD_REQUEST)


        serializer = self.serializer_class(queryset)
        return Response(serializer.data)


class ConfirmPaymentView(views.APIView):
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        data = request.data
        if data.get('payment_type') and data.get('cart'):
            try:
                cart = Cart.objects.get(id=data.get('cart'), is_pendent=True)
            except Cart.DoesNotExist:
                return Response({'message': 'o carrinho está vázio'}, status=status.HTTP_400_BAD_REQUEST)

            total = cart.courses.values('price').aggregate(total=Sum('price'))['total']
        
            queryset = self.queryset.create(
                    total=total, 
                    payment_type=data.get('payment_type'), 
                    profile=request.user,
                    cart=cart)
            serializer = self.serializer_class(queryset)
            cart.is_pendent = False
            cart.save()

            return Response(serializer.data)
        
        return Response({'message': 'Operação inválida'}, status=status.HTTP_400_BAD_REQUEST)


class PurchasesHistoricView(views.APIView):
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        payments = self.queryset.filter(profile=request.user)
        serializer = self.serializer_class(payments, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

