from django.db import models
from django.utils.timezone import now
from base_course.models import Course

from base_profile.models import Profile


class Cart(models.Model):
    is_pendent = models.BooleanField(default=True)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='cart_profile')
    courses = models.ManyToManyField(Course, through='CartCourse')


class CartCourse(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)


class Payment(models.Model):
    class PaymentType(models.IntegerChoices):
        CREDIT_CARD = 1, 'Credit Card'
        PIX = 2, 'Pix'
        PAYPAL = 3, 'Paypal'
        BANK_SLIP = 4, 'Bank Slip'

    cart = models.ForeignKey(Cart, on_delete=models.SET_NULL, null=True)
    profile = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True, blank=True)
    payment_type = models.IntegerField(default=PaymentType.PIX, choices=PaymentType.choices)
    total = models.FloatField()
    created_at = models.DateTimeField(default=now)
