from django.apps import AppConfig


class BaseCheckoutConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'base_checkout'
