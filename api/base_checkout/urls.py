from django.urls import path

from . import views

urlpatterns = [
    path('cart/', views.CartView.as_view()),
    path('payment/', views.ConfirmPaymentView.as_view()),
    path('purchases/', views.PurchasesHistoricView.as_view()),
]

