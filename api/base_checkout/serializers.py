from rest_framework import serializers
from django.db.models import Sum, Count
from .models import Cart, Payment


class CartSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    price = serializers.SerializerMethodField()
    old_price = serializers.SerializerMethodField()
    courses = serializers.SerializerMethodField()
    
    def get_price(self, obj):
        price = obj.courses.values('price').aggregate(total=Sum('price'))['total']
        return price
    
    def get_old_price(self, obj):
        old_price = obj.courses.values('old_price').aggregate(total=Sum('old_price'))['total']
        return old_price

    def get_courses(self, obj):
        courses = obj.courses.all()
        courses_list = [] 
        for course in courses:
            json = {}
            json['id'] = course.id
            json['name'] = course.name
            json['profile'] = course.profile.full_name if course.profile else ''
            json['level'] = course.get_level_display()
            json['price'] = course.price
            json['old_price'] = course.old_price
            json['materials'] = course.material_quantity 
            courses_list.append(json)
        return courses_list


class PaymentSerializer(serializers.ModelSerializer):
    courses = serializers.SerializerMethodField()
     

    class Meta:
        model = Payment
        fields = '__all__'

    def get_courses(self, obj):
        if obj.cart:
            return obj.cart.courses.values('name', 'price')
