# Generated by Django 4.0.6 on 2022-08-04 23:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base_course', '0004_course_level'),
    ]

    operations = [
        migrations.RenameField(
            model_name='course',
            old_name='odl_price',
            new_name='old_price',
        ),
    ]
