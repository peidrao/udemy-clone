from rest_framework import generics, status, views
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django.db.models import Q
from base_checkout.models import Payment

from base_course.permissions import IsResponsiblePermission, IsTeacherPermission
from base_profile.serializers import ProfileSerializer
from base_profile.models import Profile

from .serializers import CategorySerializer, CourseSerializer, CourseSubscriberSerializer, QuestionSerializer, RatingSerializer, SectionSerializer, MaterialSerializer
from .models import Course, Category, Material, Question, Rating, Section


class CourseListView(generics.ListCreateAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    permission_classes = (IsAuthenticated, IsTeacherPermission)

    def get_queryset(self):
        queryset = self.queryset.all()
        language = self.request.query_params.get('language')
        level = self.request.query_params.get('level')
        price = self.request.query_params.get('price')
        q = self.request.query_params.get('q')

        if q:
            queryset = queryset.filter(Q(name__contains=q) | Q(description__contains=q))

        if price:
           queryset = queryset.filter(price__lte=price)

        if level:
            queryset = queryset.filter(level=level)

        if language:
            queryset = queryset.filter(language=language)

        return queryset

    def create(self, request, *args, **kwargs):
        category = request.data.get('category')
        if not category:
            return Response({'message': 'Você precisa selecionar uma categoria'}, status=status.HTTP_400_BAD_REQUEST)
        
        serializer = self.serializer_class(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CourseDetailsView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    permission_classes = (IsAuthenticated, IsResponsiblePermission, IsTeacherPermission)


class CategoryListView(generics.ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class InstructorCoursesView(generics.ListAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset = self.queryset.filter(profile=self.request.user)
        return queryset 
        

class InstructorView(generics.ListAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset = self.queryset.filter(type_user=1)
        return queryset


class SectionListCreateView(generics.ListCreateAPIView):
    queryset = Section.objects.all()
    serializer_class = SectionSerializer
    permission_classes = (IsAuthenticated, IsTeacherPermission)


class SectionDetailsView(generics.ListCreateAPIView):
    queryset = Section.objects.all()
    serializer_class = SectionSerializer
    permission_classes = (IsAuthenticated, IsTeacherPermission, IsResponsiblePermission)


class MaterialListCreateView(generics.ListCreateAPIView):
    queryset = Material.objects.all()
    serializer_class = MaterialSerializer
    permission_classes = (IsAuthenticated, IsTeacherPermission)


class MaterialDetailsView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Material.objects.all()
    serializer_class = MaterialSerializer
    permission_classes = (IsAuthenticated, IsTeacherPermission)


class MeCoursesView(views.APIView):
    queryset = Course.objects.all()
    serializer_class = CourseSubscriberSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        courses_list = []
        payments = Payment.objects.filter(profile=request.user)
        for i in payments:
            courses_list.extend(list(i.cart.courses.values_list('id', flat=True)))
        
        courses_list = list(set(courses_list))
        
        queryset = self.queryset.filter(id__in=courses_list)

        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)


class RatingCourseView(views.APIView):
    queryset = Rating.objects.all()
    serializer_class = RatingSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        if Payment.objects.filter(profile=request.user, cart__courses__id=request.data.get('course')).exists():
            if Rating.objects.filter(profile=request.user, course=request.data.get('course')).exists():
                return Response({'message': 'you have already rated this course'})

            if not (request.data.get('rating') >= 1 and request.data.get('rating') <= 5):
                return Response({'message': 'rate from 1 to 5'})

            serializer = self.serializer_class(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
                 
        else:
            return Response({'message': "you can't rate this course"})


class QuestionView(views.APIView):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, course_id, material_id):
        if Payment.objects.filter(profile=request.user, cart__courses__id=course_id).exists():
            try:
                material = Material.objects.get(id=material_id)
            except Material.DoesNotExist:
                return Response({'message': 'material doesn exist'}, status=status.HTTP_404_NOT_FOUND)
            
            serializer = self.serializer_class(data=request.data, context={'material': material, 'profile': request.user})
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




        else:
            return Response({'message': "you can't rate this course"})




    