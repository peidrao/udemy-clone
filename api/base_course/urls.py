
from django.urls import path

from .views import (CategoryListView, CourseDetailsView, CourseListView, 
                    InstructorCoursesView, InstructorView, QuestionView, RatingCourseView, SectionListCreateView,
                    MaterialListCreateView, MaterialDetailsView, SectionDetailsView,
                    MeCoursesView)


urlpatterns = [
    path('courses/', CourseListView.as_view()),
    path('categories/', CategoryListView.as_view()),
    path('courses/<int:pk>/', CourseDetailsView.as_view()),
    path('my_courses/', InstructorCoursesView.as_view()),
    path('instructors/', InstructorView.as_view()),
    
    path('sections/', SectionListCreateView.as_view()),
    path('sections/<int:pk>/', SectionDetailsView.as_view()),
    
    path('material/', MaterialListCreateView.as_view()),
    path('material/<int:pk>', MaterialDetailsView.as_view()),

    path('me_courses/', MeCoursesView.as_view()),
    path('rating/', RatingCourseView.as_view()),
    path('<int:course_id>/material/<int:material_id>/', QuestionView.as_view()),

]
