from django.apps import AppConfig


class BaseCourseConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'base_course'
