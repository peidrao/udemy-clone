from time import time
from django.db import models
from django.utils import timezone
from django.db.models import Count

from base_profile.models import Profile

class Category(models.Model):
    name = models.CharField(max_length=100, null=False)
    origin = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self) -> str:
        return self.name


class Course(models.Model):
    class LanguageType(models.IntegerChoices):
        PORTUGUESE = 1, 'Portuguese'
        ENGLISH = 2, 'English'
        SPANISH = 3, 'Spanish'

    class LevelType(models.IntegerChoices):
        BEGINNER = 1, 'Beginner'
        INTERMEDIARY = 2, 'Intermediary'
        SPECIALIST = 3, 'Specialist'

    name = models.CharField(max_length=150)
    description = models.TextField()
    requirements = models.TextField()
    category = models.ForeignKey(Category, on_delete
            =models.SET_NULL, null=True, blank=True)
    profile = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True, blank=True)

    is_archived = models.BooleanField(default=False)
    level  = models.IntegerField(choices=LevelType.choices, default=LevelType.BEGINNER)
    created_at = models.DateTimeField(default=timezone.now)
    updatet_at = models.DateTimeField(null=True)

    price = models.FloatField()
    old_price = models.FloatField(null=True)

    language = models.IntegerField(choices=LanguageType.choices, default=LanguageType.PORTUGUESE)

    @property
    def material_quantity(self):
        return self.section_course.filter(material_section__isnull=False
                    ).distinct().values('material_section').aggregate(total=Count('material_section'))['total']


class Section(models.Model):
    title = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)
    course = models.ForeignKey(Course, on_delete=models.SET_NULL, null=True, related_name='section_course')
    created_at = models.DateTimeField(default=timezone.now)


class Material(models.Model):
    class MaterialType(models.IntegerChoices):
        TEXT = 1, 'Text'
        VIDEO = 2, 'Video'

    
    title = models.CharField(max_length=150)
    type_material = models.IntegerField(default=MaterialType.TEXT, choices=MaterialType.choices)
    text = models.TextField(null=True)
    video = models.FileField(upload_to='materials/', null=True)
    section = models.ForeignKey(Section, on_delete=models.SET_NULL, null=True, related_name='material_section')


class Rating(models.Model):
    rating = models.IntegerField(null=False)
    comment = models.CharField(max_length=255, null=True)
    course = models.ForeignKey(Course, on_delete=models.SET_NULL, null=True)
    profile = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(default=timezone.now)


class Question(models.Model):
    material = models.ForeignKey(Material, on_delete=models.SET_NULL, null=True, related_name='question_material')
    profile = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True, related_name='question_material')
    title = models.CharField(max_length=100)
    description = models.TextField()
    created_at = models.DateTimeField(default=timezone.now)
    