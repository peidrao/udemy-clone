from rest_framework.permissions import BasePermission, SAFE_METHODS


class IsResponsiblePermission(BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True

        return request.user == obj.profile


class IsTeacherPermission(BasePermission):
    message = 'Somente professores tem permissão para essa ação'

    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        if request.user.type_user == 1: 
                return True
        return False

