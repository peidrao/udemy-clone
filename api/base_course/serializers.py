from django.forms import model_to_dict
from rest_framework import serializers
from django.db.models import F, Avg

from .models import Category, Course, Material, Question, Rating, Section


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class CourseSerializer(serializers.ModelSerializer):
    category = serializers.SerializerMethodField()
    level = serializers.SerializerMethodField()
    sections = serializers.SerializerMethodField()
    ratings = serializers.SerializerMethodField()
    rating = serializers.SerializerMethodField()

    class Meta:
        fields = ('id', 'name', 'description', 'price', 'old_price', 'level', 'category', 'sections', 'ratings', 'rating')
        model = Course

    def create(self, validated_data):
        instance =  super(CourseSerializer, self).create(validated_data)
        instance.profile = self.context['request'].user
        instance.category_id  = self.context['request'].data.get('category')
        instance.save()
        return instance
 
    def get_category(self, obj):
        if obj.category:
            return model_to_dict(obj.category, fields=['id', 'name'])

    def get_level(self, obj):
        if obj.level:
            return obj.get_level_display()

    def get_sections(self, obj):
        sections = obj.section_course.all()
        sections_list = []
        for section in sections:
            json_section = {}
            materials_list = []
            materials = section.material_section.all()
            for material in materials:
                json = {}
                json['id'] = material.id
                json['title'] = material.title
                json['type'] = material.get_type_material_display()
                if material.video and material.video.url:            
                    json['material'] = material.video.url
                else:
                    json['material'] = material.text
                materials_list.append(json)
            json_section['id'] = section.id
            json_section['title'] = section.title
            json_section['is_active'] = section.is_active
            json_section['materials'] = materials_list
            sections_list.append(json_section)

        return sections_list

    def get_ratings(self, obj):
        ratings = obj.rating_set.values('profile__full_name').annotate(full_name=F('profile__full_name')).values(
            'id', 'full_name', 'rating', 'comment'
        )
        return ratings
    
    def get_rating(self, obj):
        rating = obj.rating_set.values('rating').aggregate(average=Avg('rating'))['average']
        return rating


class SectionSerializer(serializers.ModelSerializer):
    materials = serializers.SerializerMethodField()
    
    class Meta:
        model = Section
        fields = '__all__'

    def get_materials(self, obj):
        materials = obj.material_section.all()
        materiasl_list = []
        for material in materials:
            json = {}
            json['id'] = material.id
            json['title'] = material.title
            json['type'] = material.get_type_material_display()
            if material.video and material.video.url:            
                json['material'] = material.video.url
            else:
                json['material'] = material.text

            materiasl_list.append(json)

        return materiasl_list


class MaterialSerializer(serializers.ModelSerializer):
    questions = serializers.SerializerMethodField()

    class Meta:
        model = Material
        fields = '__all__'

    def get_questions(self, obj):
        questions = obj.question_material.values('profile').annotate(
            full_name=F('profile__full_name')).values('id', 'title', 'description', 'profile', 'full_name', 'created_at')
        return questions
        

class CourseSubscriberSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = '__all__'


class RatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rating
        fields = '__all__'

    def create(self, validated_data):
        instance = super(RatingSerializer, self).create(validated_data)
        return instance


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = '__all__'

    def create(self, validated_data):
        profile = self.context['profile']
        material = self.context['material']
        instance =  super(QuestionSerializer, self).create(validated_data)
        instance.profile = profile
        instance.material = material
        instance.save()
        return instance
