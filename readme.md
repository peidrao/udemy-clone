# Estrutura 

1. Criação do projeto
  - core do projeto.
  - requirements.txt
  - .gitignore
  - Makefile

2. Criar primeiro app (base_profile)
  - Adicionar app dentro do settings
  - Adicionar urls do base_profile dentro do settings

3. Criar primeiro modelo (Profile)
  - Explicar sobre o AbstractBaseUser
  - Explicar sobre o manager
  - Nova forma de escrever os choices
  - Adicionar modelo dentro do settings (usuário padrão)

4. Criando superusuário
  - Sobre a criação de comandos customizados
  - python manage.py createadmin

5. Serviço do usuário
  - Listagem de usuários (criar view e url)
  - apresentar ferramenta beekeeper
  
6. Instalação de JWT Token
  - instalar biblioteca 
  - ajustes em settings.py
  - Testar rota de login
  - Coisas interessantes a se fazer no insomnia

7. App de curso
  - Criar modelo de categoria
  - Criar comando para criar algumas categorias
  - Criar serializer / view / url de categorias

8. Criar modelo de curso
  - View / Serializer / url
  - Explicar sobre algumas validações (precisa ter uma categoria para ser criada)

9. Criar filtros dentro do serviço de listagem de cursos
  - criar método get_queryset 
  - Explicar LTE (menor ou igual)
  - Adicionando paginação
  - Remover campos desnecessários dentro do serviço
    - Só preciso do nome, descrição, preço, preço antigo (se tiver) e o nível

10. Conectar API com o PostgreSQL
  - Adicionar arquivo docker-compose
  - Fazer ajustes no settings

11. Edição de Curso
  - Criar permissão (somente o criador do curso pode fazer a edição)
  - Serviço

12. Permissão para apenas instrutores poderem editar ou criar cursos
  - Endpoint para listagem de cursos de um instrutor.
  - Endpoint para listagem de cursos de um instrutor

13. Criação de seções (seções serão onde as aulas ficarão organizadas)
  - Também será criado o modelo de aula
    - O nome do modelo será Material (visto que nem sempre são colocados vídeos na aula.)

14. Customizando serializer de curso
  - Será preciso adicionar a listagem de seções e os materiais que existem dentro dessas seções.

??. Adicionando pytest
  - Instalar o pytest no diretório onde se encontra a pasta venv (para não ter problemas)
  - verificar se o pytest está setado para o nosso ambiente (pytest -VV)
  - pip install pytest-django
  - criar arquivo pytest.ini
  - rodar set export DJANGO_SETTINGS_MODULE=mysite.settings no 
  - Instalando o model_bakery (pip install model_bakery)


??. Allure pytest
  - pip install allure-pytest
  - mkdir reports
  - pytest --alluredir=reports
  - https://stackoverflow.com/questions/43875741/allure-command-not-found-on-linux
  - allure serve reports 

??. Install plugins
  - pip install pytest-sugar
  - pytest -p no:sugar


??. Instalar swagger

??. Instalar o coverage
  - pip install coverage
  - coverage run -m pytest